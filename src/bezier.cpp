#include "bezier.h"


const Matrix CubicBezierCurve::s_basisMat = Matrix(
	1, -3,  3, -1,
	0,  3, -6,  3,
	0,  0,  3, -3,
	0,  0,  0,  1
);

CubicBezierCurve::CubicBezierCurve() : bSetControlPoints(false)
{
}

void CubicBezierCurve::SetControlPoints(std::array<Vector2, 4>& p)
{
	bool bSame = true;
	for (auto& point : p)
	{
		for (auto& existingPoint : controlPoints)
		{
			if (point != existingPoint)
			{
				bSame = false;
				break;
			}
		}
	}

	if (bSame) return;

    memcpy(controlPoints, &p, sizeof(int) * 4);

	// Geometry matrix
	// G = [P0 P1 P2 P3] = P0x P1x P2x P3x
	//                     P1y   ...
	Matrix3x4 geomMatrix(p[0].x, p[1].x, p[2].x, p[3].x,
						 p[0].y, p[1].y, p[2].y, p[3].y,
						 0     , 0     , 0     , 0     );

	gmMatrix = geomMatrix * s_basisMat;
	bSetControlPoints = true;
}

void CubicBezierCurve::GetSamples(Vector2 outSamples[], u32 numSamples)
{
    if(!bSetControlPoints)
    {
        LOG_CONSOLE("Attempting to sample a bezier curve without control points.");
        return;
    }

	// Sample(t) = g * m * [1 t t^2 t^3] = gm * [1 t t^2 t^3]
	for (u32 sample = 0; sample < numSamples; ++sample)
	{
		float t = 1.0f / (float)numSamples * (float)sample;
		float tmat[4] = { 1, t, t*t, t*t*t };

		for (int i = 0; i < 4; ++i)
		{
            outSamples[sample].x += gmMatrix(0, i) * tmat[i];
            outSamples[sample].y += gmMatrix(1, i) * tmat[i];
		}
	}
}

Vector2 CubicBezierCurve::GetSample(float t)
{
    Vector2 sample;

    if(!bSetControlPoints)
    {
        LOG_CONSOLE("Attempting to sample a bezier curve without control points.");
        return sample;
    }

    if(t < 0 || t > 1)
    {
        LOG_CONSOLE("Invalid curve sample location: %f. It must be in the [0..1] range.", t);
        return sample;
    }

	// Sample(t) = g * m * [1 t t^2 t^3] = gm * [1 t t^2 t^3]
	float tmat[4] = { 1, t, t*t, t*t*t };

	for (int i = 0; i < 4; ++i)
	{
		sample.x += gmMatrix(0, i) * tmat[i];
		sample.y += gmMatrix(1, i) * tmat[i];
	}

	return sample;
}

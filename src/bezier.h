#pragma once
#include "matrix.h"
#include "vector.h"
#include "common.h"
#include <array>


struct CubicBezierCurve
{
    CubicBezierCurve();

    // Array of 4 control points
    void SetControlPoints(std::array<Vector2, 4>& controlPoints);

    // Assumes valid control points have been set.
    void GetSamples(Vector2* outSamples, u32 numSamples);

    // Assumes valid control points have been set.
    // t is in the [0..1] range
    Vector2 GetSample(float t);

private:
    bool bSetControlPoints;
    Vector2 controlPoints[4];

    // Cached after setting valid control points.
	// gm = G * M                                                      3x4
	// G = geometry matrix dependent on control points                 3x4
	// M = (constant) basis matrix specific to cubic bezier curves     4x4
	Matrix3x4 gmMatrix;
    static const Matrix s_basisMat;
};

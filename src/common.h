#pragma once


// -----------------------------------------------------------------------------
// C++ Headers
#include <cstdint> // defines
#include <cstdlib> // malloc
#include <cstring>
#include <cassert>
#include <cstdarg>
#include <iostream>
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Emscripten specific
#include <emscripten/emscripten.h>

// Used like printf and the like
// LOG("%s %d %s", "ogles sucks ", NUM_DICK_INCHES, " inches of dick.");
#define LOG_CONSOLE(...) LogConsole(__FILE__, __LINE__, __VA_ARGS__)

inline void LogConsole(const char* file, int line, const char* format, ...)
{
	char buffer[256];

	sprintf(buffer, "%s, %d: ", file, line);

	va_list args;
	va_start(args, format);
	vsprintf(buffer + strlen(buffer), format, args);
	va_end(args);

	emscripten_log(EM_LOG_CONSOLE, buffer);

	// TODO temp
	std::cout << buffer << std::endl;
}
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
using u8 = uint8_t;
using u16 = uint16_t;
using u32 = uint32_t;
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
#define Kilobytes(x) ((x)          * 1024)
#define Megabytes(x) (Kilobytes(x) * 1024)
#define Gigabytes(x) (Megabytes(x) * 1024)
#define Terabytes(x) (Gigabytes(x) * 1024)
// -----------------------------------------------------------------------------

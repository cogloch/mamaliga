#include "common.h"
#include "context.h"


Context::Context(int width, int height)
{
    this->width = width;
    this->height = height;
}

void Resize(int width, int height)
{
    // TODO
    LOG_CONSOLE("Not implemented");
}

void Context::SwapBuffers()
{
    eglSwapBuffers(display, surface);
}

bool Context::Create(u32 flags, const char* title)
{
    int attribList[] ={
        EGL_RED_SIZE, 5,
        EGL_GREEN_SIZE, 6,
        EGL_BLUE_SIZE, 5,
        EGL_ALPHA_SIZE, (flags & ES_WINDOW_ALPHA) ? 8 : EGL_DONT_CARE,
        EGL_DEPTH_SIZE, (flags & ES_WINDOW_DEPTH) ? 8 : EGL_DONT_CARE,
        EGL_STENCIL_SIZE, (flags & ES_WINDOW_STENCIL) ? 8 : EGL_DONT_CARE,
        EGL_SAMPLE_BUFFERS, (flags & ES_WINDOW_MULTISAMPLE) ? 1 : 0
    };

    if(!CreateWindowX11(title))
    {
        LOG_CONSOLE("Failed creating window.");
        return false;
    }

    if(!CreateEGLContext(attribList))
    {
        LOG_CONSOLE("Failed creating EGL context.");
        return false;
    }

    return true;
}

bool Context::CreateWindowX11(const char* title)
{
    xDisplay = XOpenDisplay(NULL);
    if(!xDisplay)
    {
        LOG_CONSOLE("Failed opening a connection to X server.");
        return false;
    }

    Window root = DefaultRootWindow(xDisplay);

    XSetWindowAttributes wndAttrib;
    wndAttrib.event_mask = ExposureMask | PointerMotionMask | KeyPressMask;
    Window window = XCreateWindow(xDisplay, root,
                                  0, 0, width, height, 0,
                                  CopyFromParent, InputOutput,
                                  CopyFromParent, CWEventMask,
                                  &wndAttrib);

    XSetWindowAttributes chgAttrib;
    chgAttrib.override_redirect = 0;
    XChangeWindowAttributes(xDisplay, window, CWOverrideRedirect, &chgAttrib);

    XWMHints hints;
    hints.input = 1;
    hints.flags = InputHint;
    XSetWMHints(xDisplay, window, &hints);

    // Make the window visible
    XMapWindow(xDisplay, window);
    XStoreName(xDisplay, window, title);

    Atom state = XInternAtom(xDisplay, "_NET_WM_STATE", 0);

    XEvent event;
    memset(&event, 0, sizeof(event));
    event.type = ClientMessage;
    event.xclient.window = window;
    event.xclient.message_type = state;
    event.xclient.format = 32;
    event.xclient.data.l[0] = 1;
    event.xclient.data.l[1] = 0;
    XSendEvent(xDisplay, DefaultRootWindow(xDisplay), 0, SubstructureNotifyMask, &event);

    windowHandle = (EGLNativeWindowType)window;
    return true;
}

bool Context::CreateEGLContext(EGLint attribList[])
{
    // Get Display
    display = eglGetDisplay((EGLNativeDisplayType)xDisplay);
    if (display == EGL_NO_DISPLAY)
    {
        LOG_CONSOLE("Failed receving an EGL display.");
        return false;
    }

    // Initialize EGL
    int majorVersion, minorVersion;
    if (!eglInitialize(display, &majorVersion, &minorVersion))
    {
        LOG_CONSOLE("Failed initializing EGL.");
        return false;
    }

   // Get configs
   int numConfigs;
   if (!eglGetConfigs(display, NULL, 0, &numConfigs))
   {
       LOG_CONSOLE("Failed receiving EGL framebuffer configs.");
       return false;
   }

   // Choose config
   EGLConfig config;
   if (!eglChooseConfig(display, attribList, &config, 1, &numConfigs))
   {
       LOG_CONSOLE("Failed receiving EGL framebuffer configs with chosen attribs.");
       return false;
   }

   // Create a surface
   surface = eglCreateWindowSurface(display, config, (EGLNativeWindowType)windowHandle, NULL);
   if (surface == EGL_NO_SURFACE)
   {
       LOG_CONSOLE("Failed creating an EGL surface.");
       return false;
   }

   // Create a GL context
   EGLint contextAttribs[] = { EGL_CONTEXT_CLIENT_VERSION, 2, EGL_NONE, EGL_NONE };
   context = eglCreateContext(display, config, EGL_NO_CONTEXT, contextAttribs);
   if (context == EGL_NO_CONTEXT)
   {
       LOG_CONSOLE("Failed creating an EGL context.");
       return false;
   }

   // Make the context current
   if (!eglMakeCurrent(display, surface, surface, context))
   {
       LOG_CONSOLE("Failed making an EGL context active.");
       return false;
   }

   return true;
}

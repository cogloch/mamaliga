#pragma once

#include <GLES2/gl2.h>
#include <EGL/egl.h>

#include  <X11/Xlib.h>
#include  <X11/Xatom.h>
#include  <X11/Xutil.h>


// The framebuffer should have alpha
const int ES_WINDOW_ALPHA = 1;
// A depth buffer should be created
const int ES_WINDOW_DEPTH = 2;
// A stencil buffer should be created
const int ES_WINDOW_STENCIL = 4;
// A multi-sample buffer should be created
const int ES_WINDOW_MULTISAMPLE = 8;

struct Context
{
    Context();
    Context(int width, int height);
    bool Create(u32 flags = 0, const char* title = "Emscripten");

    void Resize(int width, int height);
    int GetWidth() { return width; }
    int GetHeight() { return height; }
    
    void SwapBuffers();

private:
    bool CreateWindowX11(const char* title);
    bool CreateEGLContext(EGLint attribList[]);

    int width;
    int height;

    Display* xDisplay;

    EGLNativeWindowType windowHandle;
    EGLDisplay display;
    EGLContext context;
    EGLSurface surface;
};

#include "eventmanager.h"
#include <emscripten/html5.h>


static inline const char *EvTypeToString(int evType)
{
    const char* events[] = {
        "(invalid)", "(none)", "keypress", "keydown", "keyup", "click", "mousedown", "mouseup", "dblclick", "mousemove", "wheel", "resize",
          "scroll", "blur", "focus", "focusin", "focusout", "deviceorientation", "devicemotion", "orientationchange", "fullscreenchange", "pointerlockchange",
          "visibilitychange", "touchstart", "touchend", "touchmove", "touchcancel", "gamepadconnected", "gamepaddisconnected", "beforeunload",
          "batterychargingchange", "batterylevelchange", "webglcontextlost", "webglcontextrestored", "mouseenter", "mouseleave", "mouseover", "mouseout", "(invalid)"
    };

    ++evType;
    if(evType < 0) evType = 0;
    if(evType >= sizeof(events)/sizeof(events[0])) evType = sizeof(events)/sizeof(events[0]) - 1;
    return events[evType];
}

EM_BOOL OnKey(int evType, const EmscriptenKeyboardEvent* ev, void* userData)
{
    LOG_CONSOLE("evType code %d name %s, key: \"%s\", code: \"%s\", location: %lu,%s%s%s%s repeat: %d, locale: \"%s\", char: \"%s\", charCode: %lu, keyCode: %lu, which: %lu\n",
    evType, EvTypeToString(evType), ev->key, ev->code, ev->location, ev->ctrlKey ? " CTRL" : "", ev->shiftKey ? " SHIFT" : "", ev->altKey ? " ALT" : "", ev->metaKey ? " META" : "",
    ev->repeat, ev->locale, ev->charValue, ev->charCode, ev->keyCode, ev->which);

    return 0;
}

void EventManager::Init()
{
    // TODO
    // @param1 - target element id; change to the default Webgl "#canvas"
    // @param2 - pass in a pointer to the input/event mgr
    // @param3 - use capture or not TODO what the fuck is capture https://www.w3.org/TR/2003/NOTE-DOM-Level-3-Events-20031107/events.html#Events-phases
    // @param4 - return true if the event is consumed; false if not, then the default browser event action is carried out and the event is allowed to pass on/bubble up as normal
    emscripten_set_keypress_callback(NULL, NULL, true, OnKey); // Similar to 'char' evts on win32
    emscripten_set_keydown_callback(NULL, NULL, true, OnKey);
    emscripten_set_keyup_callback(NULL, NULL, true, OnKey);
}

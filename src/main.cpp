#include "common.h"
#include "context.h"
#include "memory.h"
#include "shadermanager.h"
#include "eventmanager.h"
#include "sprite.h"
#include "projection.h"
#include "transform.h"

#include <chrono>
#include <cmath>

bool running = true;
Context context(800, 600);
ShaderManager shaderMgr;
EventManager eventMgr;
GameMemory g_memory;

Sprite willySprite;
Sprite agdgSprite;
Transform modelWilly({ -600.0f, -750.0f, 0.0f });
Transform modelAgdg({ -400.0f, -300.0f, 0.0f }, { 0.5f, 0.5f, 0.5f });

std::chrono::time_point<std::chrono::steady_clock> startTime;

void Update()
{
    modelWilly.TranslateY(1.0f);
    modelWilly.TranslateX(2.0f);
    modelWilly.Scale(1.001f);

    auto elapsedSinceStart = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - startTime).count();
    //modelAgdg.SetScale(sin(elapsedSinceStart));
}

void Draw()
{
    glUseProgram(shaderMgr.spriteShader);

    OrthoMat ortho(context.GetWidth(), context.GetHeight());
    glUniformMatrix4fv(glGetUniformLocation(shaderMgr.spriteShader, "proj"), 1, false, ortho.GetMatrix().v);

    glUniformMatrix4fv(glGetUniformLocation(shaderMgr.spriteShader, "model"), 1, false, modelWilly.GetMatrix().v);
    willySprite.Draw(glGetUniformLocation(shaderMgr.spriteShader, "tex"));

    glUniformMatrix4fv(glGetUniformLocation(shaderMgr.spriteShader, "model"), 1, false, modelAgdg.GetMatrix().v);
    agdgSprite.Draw(glGetUniformLocation(shaderMgr.spriteShader, "tex"));
}

void OnFrame()
{
    // TODO timing

    if(!running)
    {
        emscripten_cancel_main_loop();
        return;
    }

    Update();

    glClear(GL_COLOR_BUFFER_BIT);
    Draw();
    context.SwapBuffers();
}

int main()
{
    LOG_CONSOLE("Initializing");

    if(!g_memory.Init(Megabytes(1), Megabytes(1)))
    {
        LOG_CONSOLE("Memory allocator init failed.");
        return -1;
    }

    eventMgr.Init();

    if(!context.Create())
    {
        LOG_CONSOLE("Failed creating a window/context.");
        return -1;
    }

    willySprite.Init(0, 0, 512, 512, "assets/willy.png");
    agdgSprite.Init(0, 0, 800, 600, "assets/willy.png");

    glViewport(0, 0, context.GetWidth(), context.GetHeight());
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    if(!shaderMgr.Init())
    {
        LOG_CONSOLE("Failed initializing shader mgr.");
        return -1;
    }

    startTime = std::chrono::high_resolution_clock::now();

    emscripten_set_main_loop(OnFrame, 0, 1);
}

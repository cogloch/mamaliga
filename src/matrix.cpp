#include "matrix.h"


Matrix3x4::Matrix3x4()
{
    memset(m, 0, sizeof(float) * 3 * 4);
}

Matrix3x4::Matrix3x4(float m11_, float m12_, float m13_, float m14_,
				            float m21_, float m22_, float m23_, float m24_,
				            float m31_, float m32_, float m33_, float m34_)
{
	m11 = m11_; m12 = m12_; m13 = m13_; m14 = m14_;
	m21 = m21_; m22 = m22_; m23 = m23_; m24 = m24_;
	m31 = m31_; m32 = m32_; m33 = m33_; m34 = m34_;
}

float Matrix3x4::operator()(size_t row, size_t col) const
{
    return m[row][col];
}

Matrix3x4 operator*(const Matrix3x4& lhs, const Matrix& rhs)
{
    Matrix3x4 res;

    for(int i = 0; i < 3; ++i)
    {
        for(int j = 0; j < 4; ++j)
        {
            for(int k = 0; k < 4; ++k)
                res.m[i][j] += lhs.m[i][k] * rhs.m[k][j];
        }
    }

	return res;
}

Matrix::Matrix()
{
    memset(m, 0, sizeof(float) * 4 * 4);
}

Matrix::Matrix(float m11_, float m12_, float m13_, float m14_,
                      float m21_, float m22_, float m23_, float m24_,
                      float m31_, float m32_, float m33_, float m34_,
                      float m41_, float m42_, float m43_, float m44_)
{
    m11 = m11_; m12 = m12_; m13 = m13_; m14 = m14_;
	m21 = m21_; m22 = m22_; m23 = m23_; m24 = m24_;
	m31 = m31_; m32 = m32_; m33 = m33_; m34 = m34_;
	m41 = m41_; m42 = m42_; m43 = m43_; m44 = m44_;
}

Matrix Matrix::operator*(const Matrix& rhs) const
{
    Matrix res;

    for(int i = 0; i < 4; ++i)
    {
        for(int j = 0; j < 4; ++j)
        {
            for(int k = 0; k < 4; ++k)
                res.m[i][j] += m[i][k] * rhs.m[k][j];
        }
    }

    return res;
}

Matrix Matrix::GetTranspose() const
{
    return Matrix(m11, m21, m31, m41,
                  m12, m22, m32, m42,
                  m13, m23, m33, m43,
                  m14, m24, m34, m44);
}

float Matrix::operator()(u32 row, u32 col) const
{
    return m[row][col];
}

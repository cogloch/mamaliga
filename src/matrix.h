#pragma once
#include "common.h"

// TODO: Return rvalue? Is copy elision happening?
// TODO: SIMD/vectorization (does emscripten allow it?)

// 4x4 matrix
struct Matrix
{
    struct Vector { int x, y, z, w; };

    union
    {
        struct
		{
			float m11, m12, m13, m14;
			float m21, m22, m23, m24;
			float m31, m32, m33, m34;
			float m41, m42, m43, m44;
		};

        float v[16];
        float m[4][4];
        Vector M[4];
    };

    Matrix operator*(const Matrix&) const;
    Matrix GetTranspose() const;

    Matrix();
    Matrix(float m11, float m12, float m13, float m14,
		   float m21, float m22, float m23, float m24,
		   float m31, float m32, float m33, float m34,
		   float m41, float m42, float m43, float m44);

    // Returns the element at [row][col]
    float operator()(u32 row, u32 col) const;
};

struct Matrix3x4
{
	union
	{
		struct
		{
			float m11, m12, m13, m14;
			float m21, m22, m23, m24;
			float m31, m32, m33, m34;
		};

		float m[3][4];
	};

	Matrix3x4();
	Matrix3x4(float m11, float m12, float m13, float m14,
			  float m21, float m22, float m23, float m24,
		      float m31, float m32, float m33, float m34);

	float operator()(size_t row, size_t col) const;
};

Matrix3x4 operator*(const Matrix3x4&, const Matrix&);

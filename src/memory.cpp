#include "memory.h"
#include <utility>


GameMemory::Marker::Marker(bool freeOnDestr, GameMemory* allocator)
{
	this->freeOnDestr = freeOnDestr;
	this->allocator = allocator;
}

GameMemory::Marker::~Marker()
{
	if (freeOnDestr)
	{
		allocator->FreeToTransientMarker(*this);
	}
}

GameMemory::Marker GameMemory::GetTransientMarker(bool freeOnDestr)
{
	Marker marker(freeOnDestr, this);
	marker.markerLocation = transientStorageTop;
	return std::move(marker);
}

void GameMemory::FreeToTransientMarker(GameMemory::Marker& marker)
{
	transientStorageTop = marker.markerLocation;
}

bool GameMemory::Init(const u32 permanentStorageSize_, const u32 transientStorageSize_)
{
	*this = {};

	permanentStorageSize = permanentStorageSize_;
	transientStorageSize = transientStorageSize_;
	transientStorageTop = 0;

	u32 totalSize = permanentStorageSize + transientStorageSize;
	permanentStorage = malloc(totalSize);
	transientStorage = (u8*)permanentStorage + permanentStorageSize;

	if (!permanentStorage)
	{
        LOG_CONSOLE("Failed to initialize memory allocator.");
		return false;
	}

	return true;
}

bool GameMemory::s_instantiated = false;

GameMemory::GameMemory()
{
    if(s_instantiated)
    {
        LOG_CONSOLE("Attempting to create a second instance of the mem allocator.");
    }

    s_instantiated = true;
}

GameMemory::~GameMemory()
{
    s_instantiated = false;
}

#pragma once
#include "common.h"


// TODO: Split transient and permanent storage(maybe)
// No RAII going on - clean up your own shit
struct GameMemory
{
	struct Marker
	{
		Marker(bool freeOnDestr = false, GameMemory* allocator = nullptr);
		~Marker();

	private:
		friend struct GameMemory;
		u32 markerLocation;

		bool freeOnDestr;
		GameMemory* allocator;
	};

	bool Init(const u32 permanentStorageSize, const u32 transientStorageSize);

	// @num - more than 1 implies an array
	template<typename T>
	T* AllocTransient(u32 num = 1);

	Marker GetTransientMarker(bool freeOnDestr = true);
	void FreeToTransientMarker(Marker& marker);

	u32 permanentStorageSize;
	void* permanentStorage;

	// Stack allocator
	u32 transientStorageTop;
	u32 transientStorageSize;
	void* transientStorage;

    GameMemory();
    ~GameMemory();

private:
    static bool s_instantiated;
};

template<typename T>
inline T* GameMemory::AllocTransient(u32 num)
{
	u32 newTop = transientStorageTop + sizeof(T) * num;
	if (newTop > transientStorageSize)
	{
        LOG_CONSOLE("Transient memory allocation failed. Out of memory.");
		return nullptr;
	}

	transientStorageTop = newTop;
	return (T*)((uint8_t*)(transientStorage)+transientStorageTop - sizeof(T) * num);
}


// Global allocator
extern GameMemory g_memory;

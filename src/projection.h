#pragma once
#include "matrix.h"


struct ProjectionMat
{
    Matrix GetMatrix() const { return projMat; }

protected:
    Matrix projMat;
    ProjectionMat() = default;
};

struct OrthoMat : ProjectionMat
{
    OrthoMat(float width, float height, float nearPlane = 0.0f, float farPlane = 100.0f);
};

struct PerspectiveMat : ProjectionMat
{
    PerspectiveMat(float width, float height, float fovY, float nearPlane = 1.0f, float farPlane = 1000.0f);
};


inline OrthoMat::OrthoMat(float width, float height, float nearPlane, float farPlane)
{
    // 2/(r-l) 0       0        -(r+l)/(r-l)
    // 0       2/(t-b) 0        -(t+b)/(t-b)
    // 0       0       -2/(f-n) -(f+n)/(f-n)
    // 0       0       0        1
    float range = farPlane - nearPlane;
    projMat = Matrix( 2.0f/width, 0.0f       , 0.0f       , -1.0f,
         	  		  0.0f      , 2.0f/height, 0.0f       , -1.0f,
         	  		  0.0f      , 0.0f       , -2.0f/range, -(farPlane + nearPlane)/range,
                      0.0f      , 0.0f       , 0.0f       , 1.0f );
}

inline PerspectiveMat::PerspectiveMat(float width, float height, float fovY, float nearPlane, float farPlane)
{
    // TODO
}

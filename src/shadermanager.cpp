#include "shadermanager.h"
#include "memory.h"
#include <GLES2\gl2.h>


enum class ShaderType
{
    VERT = GL_VERTEX_SHADER,
    FRAG = GL_FRAGMENT_SHADER
};

bool CompileShader(ShaderType type, const char* src, u32* output)
{
    u32 shader = glCreateShader((GLenum)type);
    if(!shader)
    {
        LOG_CONSOLE("Failed creating a new shader object.");
        return false;
    }

    glShaderSource(shader, 1, &src, NULL);
    glCompileShader(shader);

    int status = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    if(!status)
    {
        int errLen = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &errLen);
        if(errLen > 1)
        {
            char* errMsg = g_memory.AllocTransient<char>(errLen);
            glGetShaderInfoLog(shader, errLen, NULL, errMsg);
            LOG_CONSOLE("Failed shader compilation: %s", errMsg);
        }

        glDeleteShader(shader);
        return false;
    }

    *output = shader;
    return true;
}

bool LinkShader(u32 numStages, u32 shaderStages[], u32* output)
{
    u32 shader = glCreateProgram();
    if(!shader)
    {
        LOG_CONSOLE("Failed shader program creation.");
        return false;
    }

    for(int i = 0; i < numStages; ++i)
        glAttachShader(shader, shaderStages[i]);
    glLinkProgram(shader);

    int status = 0;
    glGetProgramiv(shader, GL_LINK_STATUS, &status);
    if(!status)
    {
        int errLen = 0;
        glGetProgramiv(shader, GL_INFO_LOG_LENGTH, &errLen);
        if(errLen > 1)
        {
            char* errMsg = g_memory.AllocTransient<char>(errLen);
            glGetProgramInfoLog(shader, errLen, NULL, errMsg);
            LOG_CONSOLE("Failed linking shader program: %s", errMsg);
        }

        glDeleteProgram(shader);
        return false;
    }

    *output = shader;
    return true;
}

struct Stage
{
    ShaderType type;
    const char* src;
};

bool CreateShaderProgram(int numStages, Stage stages[], u32* output)
{
    auto marker = g_memory.GetTransientMarker();
    u32* compiledStages = g_memory.AllocTransient<u32>(numStages);

    for(int i = 0; i < numStages; ++i)
    {
        if(!CompileShader(stages[i].type, stages[i].src, &compiledStages[i]))
        {
            LOG_CONSOLE("Shader compilation failed.");
            return false;
        }
    }

    if(!LinkShader(numStages, compiledStages, output))
    {
        LOG_CONSOLE("Shader linking failed.");
        return false;
    }

    return true;
}

bool ShaderManager::InitSpriteShader()
{
    const char* vertShaderSource = R"(
    attribute vec4 vertex;
    varying vec2 texCoord;

    uniform mat4 proj;
    uniform mat4 model; // TODO rely only on batching instead?

    void main()
    {
        // Flip
        texCoord = vec2(vertex.z, 1.0 - vertex.w);

        //texCoord = vertex.zw;
        gl_Position = vec4((proj * model * vec4(vertex.xy, 0.0, 1.0)).xy, 0.0, 1.0);
    }
    )";

    const char* fragShaderSource = R"(
    precision mediump float;
    varying vec2 texCoord;
    uniform sampler2D tex;
    void main()
    {
        gl_FragColor = texture2D(tex, texCoord);
    }
    )";

    Stage stages[] = {
        { ShaderType::VERT, vertShaderSource },
        { ShaderType::FRAG, fragShaderSource }
    };
    return CreateShaderProgram(2, stages, &spriteShader);
}

bool ShaderManager::s_instantiated = false;

ShaderManager::ShaderManager()
{
    if(s_instantiated)
    {
        LOG_CONSOLE("Attempting to create a second instance of the shader mgr.");
    }

    s_instantiated = true;
}

ShaderManager::~ShaderManager()
{
    s_instantiated = false;
}

bool ShaderManager::Init()
{
    if(!InitSpriteShader()) return false;
    return true;
}

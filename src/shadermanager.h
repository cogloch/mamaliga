#pragma once
#include "common.h"


struct ShaderManager
{
    ShaderManager();
    ~ShaderManager();
	bool Init();

    u32 spriteShader;

private:
    static bool s_instantiated;

    bool InitSpriteShader();
};

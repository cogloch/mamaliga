#include "sprite.h"
#include "memory.h"
#include <GLES2/gl2.h>

u32 Sprite::s_iboId = 0;

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
bool LoadTexture(const char* filename, u32* output)
{
    u32 tex;

    int width, height, comp;
    u8* buffer = stbi_load(filename, &width, &height, &comp, 0);

    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);

    if(comp == 3)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, buffer);
    }
    else if(comp == 4)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
    }
    else
    {
        LOG_CONSOLE("Invalid component count: %d", comp); // TODO grayscale?
        glDeleteTextures(1, &tex);
        return false;
    }

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    *output = tex;
    return true;
}

Sprite::Sprite()
{
    vboId = 0;
}

Sprite::~Sprite()
{
    if(vboId)
    {
        glDeleteBuffers(1, &vboId);
    }
}

void Sprite::Reset()
{
    if(!vboId)
    {
        LOG_CONSOLE("Attempting to reset a sprite object that has not been initialized.");
        return;
    }

    glDeleteBuffers(1, &vboId);
}

bool Sprite::Init(float x, float y, float width, float height, const char* texturePath)
{
    if(vboId)
    {
        LOG_CONSOLE("Use Reset() on the sprite object to reuse it.");
        return false;
    }

    if(!s_iboId)
    {
        u16 indices[] = { 0, 1, 2, 0, 2, 3 };
        glGenBuffers(1, &s_iboId);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, s_iboId);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    this->x = x;
    this->y = y;
    this->width = width;
    this->height = height;

    // TODO resource caching
    if(!LoadTexture(texturePath, &textureId))
    {
        LOG_CONSOLE("Failed loading texture %s", texturePath);
        return false;
    }

    float verts[] = {
        x,         y,                0.0f, 0.0f,
        x,         y + height,       0.0f, 1.0f,
        x + width, y + height,       1.0f, 1.0f,
        x + width, y,                1.0f, 0.0f
    };

    glGenBuffers(1, &vboId);
    glBindBuffer(GL_ARRAY_BUFFER, vboId);
    glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);

    return true;
}

void Sprite::Draw(u32 samplerLoc)
{
    glBindBuffer(GL_ARRAY_BUFFER, vboId);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * 4, 0);
    glEnableVertexAttribArray(0);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureId);
    glUniform1i(glGetUniformLocation(samplerLoc, "tex"), 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, s_iboId);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);
}

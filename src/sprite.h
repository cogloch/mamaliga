#pragma once
#include "common.h"


struct Sprite
{
    Sprite();
    ~Sprite();

    bool Init(float x, float y, float width, float height, const char* texturePath);
    void Reset();

    // Assumes the sprite shader and projection matrix are bound
    void Draw(u32 samplerLoc);

    float x, y;
    float width, height;
    u32 vboId;
    u32 textureId;

    static u32 s_iboId;
};

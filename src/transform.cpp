#include "transform.h"


Transform::Transform()
	: scale(1.0f, 1.0f, 1.0f)
{
	CacheTransform();
}

Transform::Transform(const Vector3& position)
	: TransformNoScale(position)
	, scale(1.0f, 1.0f, 1.0f)
{
	CacheTransform();
}

Transform::Transform(const Vector3& position, const Vector3& scale_)
	: TransformNoScale(position)
	, scale(scale_)
{
	CacheTransform();
}

bool operator==(const Transform& lhs, const Transform& rhs)
{
    for (int row = 0; row < 4; ++row)
	{
		for (int col = 0; col < 4; ++col)
		{
			if (lhs.transformMat(row, col) != rhs.transformMat(row, col))
				return false;
		}
	}

	return true;
}

bool operator!=(const Transform& lhs, const Transform& rhs)
{
    for (int row = 0; row < 4; ++row)
	{
		for (int col = 0; col < 4; ++col)
		{
			if (lhs.transformMat(row, col) != rhs.transformMat(row, col))
				return true;
		}
	}

	return false;
}

void Transform::SetScale(float byUniformScale)
{
	scale = { byUniformScale, byUniformScale, byUniformScale };
	CacheTransform();
}

void Transform::SetScale(const Vector3& newScale)
{
	scale = newScale;
	CacheTransform();
}

void Transform::SetScale(float x, float y, float z)
{
	scale = { x, y, z };
	CacheTransform();
}

void Transform::Scale(float uniformScale)
{
    scale *= uniformScale;
	CacheTransform();
}

void Transform::Scale(const Vector3& dScale)
{
	scale.x *= dScale.x;
	scale.y *= dScale.y;
	scale.z *= dScale.z;
	CacheTransform();
}

void Transform::Scale(float byX, float byY, float byZ)
{
	scale.x *= byX;
	scale.y *= byY;
	scale.z *= byZ;
	CacheTransform();
}

void Transform::ScaleX(float byX)
{
	scale.x *= byX;
	CacheTransform();
}

void Transform::ScaleY(float byY)
{
	scale.y *= byY;
	CacheTransform();
}

void Transform::ScaleZ(float byZ)
{
	scale.z *= byZ;
	CacheTransform();
}

void Transform::CacheTransform()
{
    Matrix translationMat = {
        1.0f,       0.0f,       0.0f,       0.0f,
        0.0f,       1.0f,       0.0f,       0.0f,
        0.0f,       0.0f,       1.0f,       0.0f,
        position.x, position.y, position.z, 1.0f
    };

    Matrix scaleMat = {
        scale.x, 0.0f,    0.0f,    0.0f,
           0.0f, scale.y, 0.0f,    0.0f,
           0.0f, 0.0f,    scale.z, 0.0f,
           0.0f, 0.0f,    0.0f,    1.0f
    };

    // Scale -> Translate
    transformMat = translationMat * scaleMat;
}

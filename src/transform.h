#pragma once
#include "transformnoscale.h"

struct Transform : TransformNoScale
{
	Transform();
	Transform(const Vector3& position);
	Transform(const Vector3& position, const Vector3& scale);

	const Vector3& GetScale() const { return scale; }

	// TODO transform concatenation

	friend bool operator==(const Transform&, const Transform&);
	friend bool operator!=(const Transform&, const Transform&);

	// Absolute values
	void SetScale(float uniformScale);
	void SetScale(const Vector3&);
	void SetScale(float x, float y, float z);

	// Relative values
	void Scale(float byUniformScale);
	void Scale(const Vector3&);
	void Scale(float byX, float byY, float byZ);
	void ScaleX(float byX);
	void ScaleY(float byY);
	void ScaleZ(float byZ);

protected:
	void CacheTransform() override;

private:
	Vector3 scale;
};

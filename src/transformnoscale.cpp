#include "transformnoscale.h"


const TransformNoScale TransformNoScale::defaultTransform = TransformNoScale();

TransformNoScale::TransformNoScale()
	: position(0.0f, 0.0f, 0.0f)
{
	CacheTransform();
}

TransformNoScale::TransformNoScale(const Vector3& position_)
    : position(position_)
{
    CacheTransform();
}

bool operator==(const TransformNoScale& lhs, const TransformNoScale& rhs)
{
	for (int row = 0; row < 4; ++row)
	{
		for (int col = 0; col < 4; ++col)
		{
			if (lhs.transformMat(row, col) != rhs.transformMat(row, col))
				return false;
		}
	}

	return true;
}

bool operator!=(const TransformNoScale& lhs, const TransformNoScale& rhs)
{
    for (int row = 0; row < 4; ++row)
	{
		for (int col = 0; col < 4; ++col)
		{
			if (lhs.transformMat(row, col) != rhs.transformMat(row, col))
				return true;
		}
	}

	return false;
}

void TransformNoScale::SetPosition(const Vector3& newPosition)
{
	position = newPosition;
	CacheTransform();
}

void TransformNoScale::SetPosition(float x, float y, float z)
{
	position = { x, y, z };
	CacheTransform();
}

void TransformNoScale::Translate(const Vector3& dPosition)
{
    position += dPosition;
	CacheTransform();
}

void TransformNoScale::Translate(float dX, float dY, float dZ)
{
	position.x += dX;
	position.y += dY;
	position.z += dZ;
	CacheTransform();
}

void TransformNoScale::TranslateX(float dX)
{
	position.x += dX;
	CacheTransform();
}

void TransformNoScale::TranslateY(float dY)
{
	position.y += dY;
	CacheTransform();
}

void TransformNoScale::TranslateZ(float dZ)
{
	position.z += dZ;
	CacheTransform();
}

void TransformNoScale::CacheTransform()
{
    Matrix translation = {
        1.0f,       0.0f,       0.0f,       0.0f,
        0.0f,       1.0f,       0.0f,       0.0f,
        0.0f,       0.0f,       1.0f,       0.0f,
        position.x, position.y, position.z, 1.0f
    };

    transformMat = translation;
}

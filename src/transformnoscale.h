#pragma once
#include "matrix.h"
#include "vector.h"


struct TransformNoScale
{
	TransformNoScale();
	TransformNoScale(const Vector3& position);

	virtual ~TransformNoScale() = default;

	const Matrix& GetMatrix() const { return transformMat; }
	const Vector3& GetPosition() const { return position; }

	// TODO transform concatenation

	friend bool operator==(const TransformNoScale&, const TransformNoScale&);
	friend bool operator!=(const TransformNoScale&, const TransformNoScale&);

	// Absolute values
	void SetPosition(const Vector3&);
	void SetPosition(float x, float y, float z);

	// Relative values
	void Translate(const Vector3&);
	void Translate(float dX, float dY, float dZ);
	void TranslateX(float dX);
	void TranslateY(float dY);
	void TranslateZ(float dZ);

	// Position (0, 0, 0)
	static const TransformNoScale defaultTransform;

protected:
	virtual void CacheTransform();
	Matrix transformMat;
	Vector3 position;
};

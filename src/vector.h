#pragma once
#include <cmath>
#include "common.h"


struct Vector2
{
	union
	{
		struct
		{
			float x;
			float y;
		};

		float val[2];
	};

	// Set all components to 0
	Vector2();

	// Set the x and y components to the passed @x and @y
	Vector2(float x, float y);
	explicit Vector2(int x, int y);
	explicit Vector2(u32 x, u32 y);

	// Set all components to @uniformValue
	Vector2(float uniformValue);

	Vector2 operator+=(const Vector2&);
	Vector2 operator*=(float scale);
};

struct Vector3
{
	union
	{
		struct
		{
			float x;
			float y;
			float z;
		};

		float val[3];
	};

	// Set all components to 0
	Vector3();

	// Set the x, y and z components to the passed @x, @y and @z
	Vector3(float x, float y, float z);

	// Set all components to @uniformValue
	Vector3(float uniformValue);

	// Set x and y to @vec's x and y and the z component to @z
	Vector3(Vector2 vec, float z = 0.0f);

	Vector3& operator=(const Vector3&);
	Vector3 operator+(const Vector3&) const;
	Vector3 operator-(const Vector3&) const;
	Vector3 operator*(float scale) const;
	Vector3 operator/(float scale) const;
	Vector3 operator+=(const Vector3&);
	Vector3 operator-=(const Vector3&);
	Vector3 operator*=(float scale);
	Vector3 operator/=(float scale);

	float GetMagnitude() const;
	float GetMagnitudeSq() const ;
	void Normalize();
};

inline Vector2::Vector2() : x(0.0f), y(0.0f) {}
inline Vector2::Vector2(float x_, float y_) : x(x_), y(y_) {}
inline Vector2::Vector2(int x_, int y_) : x(static_cast<float>(x_)), y(static_cast<float>(y_)) {}
inline Vector2::Vector2(u32 x_, u32 y_) : x(static_cast<float>(x_)), y(static_cast<float>(y_)) {}
inline Vector2::Vector2(float uniformValue) : x(uniformValue), y(uniformValue) {}

inline Vector2 Vector2::operator+=(const Vector2& other)
{
	x += other.x;
	y += other.y;
	return *this;
}

inline Vector2 Vector2::operator*=(float scale)
{
	x *= scale;
	y *= scale;
	return *this;
}

inline bool operator!=(const Vector2& lhs, const Vector2& rhs)
{
	return !(lhs.x == rhs.x && lhs.y == rhs.y);
}

inline bool operator!=(const Vector3& lhs, const Vector3& rhs)
{
	return !(lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z);
}

inline bool operator==(const Vector2& lhs, const Vector2& rhs)
{
	return (lhs.x == rhs.x && lhs.y == rhs.y);
}

inline bool operator==(const Vector3&lhs , const Vector3& rhs)
{
	return (lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z);
}

inline Vector3::Vector3() : x(0.0f), y(0.0f), z(0.0f) {}
inline Vector3::Vector3(float x_, float y_, float z_) : x(x_), y(y_), z(z_) {}
inline Vector3::Vector3(float uniformValue) : x(uniformValue), y(uniformValue), z(uniformValue) {}
inline Vector3::Vector3(Vector2 vec, float z_) : x(vec.x), y(vec.y), z(z_) {}

inline Vector3& Vector3::operator=(const Vector3& other)
{
	x = other.x;
	y = other.y;
	z = other.z;
	return *this;
}

inline Vector3 Vector3::operator+(const Vector3& other) const
{
	return Vector3(x + other.x, y + other.y, z + other.z);
}

inline Vector3 Vector3::operator-(const Vector3& other) const
{
	return Vector3(x - other.x, y - other.y, z - other.z);
}

inline Vector3 Vector3::operator*(float scale) const
{
	return Vector3(x * scale, y * scale, z * scale);
}

inline Vector3 Vector3::operator/(float scale) const
{
	return Vector3(x / scale, y / scale, z / scale);
}

inline Vector3 Vector3::operator+=(const Vector3& other)
{
	x += other.x;
	y += other.y;
	z += other.z;
	return *this;
}

inline Vector3 Vector3::operator-=(const Vector3& other)
{
	x -= other.x;
	y -= other.y;
	z -= other.z;
	return *this;
}

inline Vector3 Vector3::operator*=(float scale)
{
	x *= scale;
	y *= scale;
	z *= scale;
	return *this;
}

inline Vector3 Vector3::operator/=(float scale)
{
	x /= scale;
	y /= scale;
	z /= scale;
	return *this;
}

inline float Vector3::GetMagnitude() const
{
	return sqrtf(x*x + y*y + z*z);
}

inline float Vector3::GetMagnitudeSq() const
{
	return (x*x + y*y + z*z);
}

inline void Vector3::Normalize()
{
	float magnitude = GetMagnitude();
	if (magnitude != 0)
	{
		x /= magnitude;
		y /= magnitude;
		z /= magnitude;
	}
}

inline Vector2 operator*(float scale, const Vector2& vec)
{
	return Vector2(vec.x * scale, vec.y * scale);
}

inline Vector3 operator*(float scale, const Vector3& vec)
{
	return Vector3(vec.x * scale, vec.y * scale, vec.z * scale);
}
